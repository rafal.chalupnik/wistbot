﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WistBot.Core.Extensions
{
    internal static class EnumerableExtensions
    {
        public static List<T> Repeat<T>(Func<T> function, int times)
        {
            return Enumerable.Range(0, times)
                .Select(i => function())
                .ToList();
        }
    }
}