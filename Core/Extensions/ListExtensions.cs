﻿using System;
using System.Collections.Generic;

namespace WistBot.Core.Extensions
{
    internal static class ListExtensions
    {
        public static T PopRandom<T>(this List<T> list, Random random)
        {
            var element = list[random.Next(list.Count)];
            list.Remove(element);
            return element;
        }
    }
}