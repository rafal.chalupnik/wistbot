﻿using System;

namespace WistBot.Core.Models.Cards
{
    public class Card
    {
        public Color Color { get; }

        public Rank Rank { get; }

        public Card(Color color, Rank rank)
        {
            if (!Enum.IsDefined(typeof(Color), color))
                throw new ArgumentException("Invalid color.");

            if (!Enum.IsDefined(typeof(Rank), rank))
                throw new ArgumentException("Invalid rank.");

            Color = color;
            Rank = rank;
        }
    }
}