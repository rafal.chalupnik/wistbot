﻿using System;
using System.Collections.Generic;
using System.Linq;
using WistBot.Core.Extensions;

namespace WistBot.Core.Models.Cards
{
    public class CardDealer
    {
        private readonly Random m_random;
        private readonly List<Card> m_cards;

        public CardDealer(Random random)
        {
            m_random = random;
            m_cards = GenerateCards().ToList();
        }

        public bool IsEmpty => m_cards.Count == 0;

        public List<Card> DrawCards(int cards)
            => EnumerableExtensions.Repeat(DrawCard, cards);

        private Card DrawCard()
            => m_cards.PopRandom(m_random);

        private static IEnumerable<Card> GenerateCards()
        {
            return 
                from color 
                    in Enum.GetValues(typeof(Color)).Cast<Color>() 
                from rank 
                    in Enum.GetValues(typeof(Rank)).Cast<Rank>() 
                select new Card(color, rank);
        }
    }
}