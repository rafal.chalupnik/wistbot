﻿namespace WistBot.Core.Models.Cards
{
    public enum Color
    {
        Hearts,
        Diamonds,
        Spades,
        Clubs
    }
}