﻿using System;
using System.Collections.Generic;
using System.Linq;
using WistBot.Core.Models.Cards;

namespace WistBot.Core.Models
{
    public class CardStack
    {
        private readonly Stack<Card> m_cards;

        public Card Card => m_cards.Peek();

        public bool IsMoreUnder => m_cards.Count > 1;

        public CardStack(IEnumerable<Card> cards)
        {
            m_cards = new Stack<Card>(cards);
        }

        public Card Pop() => m_cards.Pop();
    }

    public class PlayerCardsState
    {
        public IReadOnlyCollection<CardStack> OnTable { get; }

        public IReadOnlyCollection<Card> OnHand { get; }

        public PlayerCardsState(IEnumerable<CardStack> onTable, IEnumerable<Card> onHand)
        {
            OnTable = onTable.ToList();
            OnHand = onHand.ToList();
        }

        public bool Contains(Card card, bool canPlayOnHand)
        {
            if (canPlayOnHand)
                return OnHand.Contains(card);

            return OnTable
                .Any(onTable => onTable.Card == card);
        }

        public void RemoveCard(Card card, bool canPlayOnHand)
        {

        }
    }

    public class PlayerState
    {
        public PlayerCardsState Cards { get; }

        public Card? PlayedCard { get; private set; }

        public int Points { get; }

        public PlayerState(PlayerCardsState cards)
        {
            Cards = cards;
            Points = 0;
        }

        public void PlayCard(Card card)
        {
            if (!Cards.Contains(card, canPlayOnHand: Points >= 3))
                throw new InvalidOperationException("Cannot play this card.");

            PlayedCard = card;
        }
    }

    public class GameState
    {
        public PlayerState Player1 { get; }

        public PlayerState Player2 { get; }

        public GameState(PlayerState player1, PlayerState player2)
        {
            Player1 = player1;
            Player2 = player2;
        }
    }

    public static class GameStateCreator
    {
        private const int c_cardsInStack = 2;
        private const int c_stacks = 8;
        private const int c_cardsOnHand = 10;

        public static GameState CreateGame(Random random)
        {
            var cardDealer = new CardDealer(random);

            var player1 = CreatePlayerState(cardDealer);
            var player2 = CreatePlayerState(cardDealer);

            if (!cardDealer.IsEmpty)
                throw new InvalidOperationException("There are cards left!");

            return new GameState(player1, player2);
        }

        private static PlayerState CreatePlayerState(CardDealer cardDealer)
        {
            var playerCardsState = CreatePlayerCardsState(cardDealer);
            return new PlayerState(playerCardsState);
        }

        private static PlayerCardsState CreatePlayerCardsState(CardDealer cardDealer)
        {
            var cardStacks = Enumerable.Range(0, c_stacks)
                .Select(i => CreateCardStack(cardDealer))
                .ToList();

            var cardsOnHand = cardDealer.DrawCards(c_cardsOnHand);

            return new PlayerCardsState(cardStacks, cardsOnHand);
        }

        private static CardStack CreateCardStack(CardDealer cardDealer)
        {
            return new CardStack(cardDealer.DrawCards(c_cardsInStack));
        }
    }
}