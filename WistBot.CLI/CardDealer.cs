﻿using System;
using System.Collections.Generic;
using System.Linq;
using WistBot.CLI.Constants;
using WistBot.CLI.Extensions;

namespace WistBot.CLI
{
    public interface ICardDealer
    {
        List<Card> DrawCards(int count);
    }

    public class CardDealer : ICardDealer
    {
        private readonly Random m_random;
        private readonly List<Card> m_cards;

        public CardDealer(Random random)
        {
            m_random = random;
            m_cards = GenerateCards().ToList();
        }

        public List<Card> DrawCards(int count)
            => EnumerableExtensions.Repeat(DrawCard, count);

        private Card DrawCard()
            => m_cards.PopRandom(m_random);

        private static IEnumerable<Card> GenerateCards()
        {
            return
                from color
                    in Enum.GetValues(typeof(Color)).Cast<Color>()
                from rank
                    in Enum.GetValues(typeof(Rank)).Cast<Rank>()
                select new Card(color, rank);
        }
    }
}