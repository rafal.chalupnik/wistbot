﻿namespace WistBot.CLI.Constants
{
    public enum Color
    {
        Hearts,
        Diamonds,
        Spades,
        Clubs
    }
}