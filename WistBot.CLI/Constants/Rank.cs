﻿namespace WistBot.CLI.Constants
{
    public enum Rank
    {
        Rank2 = 2,
        Rank3 = 3,
        Rank4 = 4,
        Rank5 = 5,
        Rank6 = 6,
        Rank7 = 7,
        Rank8 = 8,
        Rank9 = 9,
        Rank10 = 10,
        Jack = 11,
        Queen = 12,
        King = 13,
        Ace = 14
    }
}