﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WistBot.CLI.Extensions
{
    public static class EnumerableExtensions
    {
        public static bool IsEmpty<T>(this IEnumerable<T> enumerable)
            => !enumerable.Any();

        public static T MaxElement<T>(this IEnumerable<T> enumerable, Func<T, int> selector)
            => enumerable.Aggregate((elem1, elem2) => selector(elem1) > selector(elem2) ? elem1 : elem2);

        public static T MinElement<T>(this IEnumerable<T> enumerable, Func<T, int> selector)
            => enumerable.Aggregate((elem1, elem2) => selector(elem1) < selector(elem2) ? elem1 : elem2);

        public static List<T> Repeat<T>(Func<T> function, int times)
        {
            return Enumerable.Range(0, times)
                .Select(i => function())
                .ToList();
        }
    }
}