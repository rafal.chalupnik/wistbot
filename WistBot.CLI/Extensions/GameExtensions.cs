﻿namespace WistBot.CLI.Extensions
{
    public static class GameExtensions
    {
        public static int CalculatePointsBalance(this IGame game)
            => game.Player1.Points - game.Player2.Points;
    }
}