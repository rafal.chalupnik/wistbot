﻿using System;
using WistBot.CLI.Constants;

namespace WistBot.CLI.Extensions
{
    public static class PlayerExtensions
    {
        public static Player GetOppositePlayer(this Player player)
        {
            switch (player)
            {
                case Player.Player1:
                    return Player.Player2;
                case Player.Player2:
                    return Player.Player1;
                default:
                    throw new InvalidOperationException("Not defined.");
            }
        }
    }
}