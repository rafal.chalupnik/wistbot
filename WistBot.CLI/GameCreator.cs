﻿using System;
using System.Linq;
using WistBot.CLI.Constants;

namespace WistBot.CLI
{
    public class GameCreator
    {
        private const int c_cardsInStack = 2;
        private const int c_stacks = 8;
        private const int c_cardsOnHand = 10;

        public static IGame CreateGame(Random random, Option<Color> triumph)
        {
            var cardDealer = new CardDealer(random);

            var player1 = CreatePlayerState(cardDealer);
            var player2 = CreatePlayerState(cardDealer);

            return new Game(
                Player.Player1,
                Option<Card>.None(),
                player1,
                player2,
                triumph);
        }

        private static IPlayerState CreatePlayerState(ICardDealer cardDealer)
        {
            var playerCards = CreatePlayerCards(cardDealer);
            return new PlayerState(playerCards);
        }

        private static IPlayerCards CreatePlayerCards(ICardDealer cardDealer)
        {
            var onTable = Enumerable.Range(0, c_stacks)
                .Select(i => CreateCardStack(cardDealer))
                .ToList();

            var onHand = cardDealer.DrawCards(c_cardsOnHand);

            return new PlayerCards(onTable, onHand);
        }

        private static ICardStack CreateCardStack(ICardDealer cardDealer)
        {
            return new CardStack(cardDealer.DrawCards(c_cardsInStack));
        }
    }
}