﻿//using System;
//using System.Linq;
//using WistBot.CLI.Constants;
//using WistBot.CLI.Extensions;

//namespace WistBot.CLI
//{
//    public interface IBot
//    {
//        Card Move(IGame game);
//    }

//    public class Bot : IBot
//    {
//        private struct Node
//        {
//            public Card Card { get; }

//            public int Points { get; }

//            public Node(int points, Card card)
//            {
//                Card = card;
//                Points = points;
//            }
//        }

//        private readonly Player m_player;

//        public Bot(Player player)
//        {
//            m_player = player;
//        }

//        public Card Move(IGame game)
//        {
//            if (game.CurrentPlayer != m_player)
//                throw new InvalidOperationException("Not my move!");

//            var node = Decide(game, null, m_player == Player.Player1);
//            return node.Card;
//        }

//        private static Node Decide(IGame game, Card? cardPlayed, bool maximize)
//        {
//            if (game.HasEnded)
//            {
//                if (cardPlayed == null)
//                    throw new InvalidOperationException("WTF?");

//                return new Node(game.CalculatePointsBalance(), cardPlayed.Value);
//            }

//            var possibleMoves = game.GetPossibleCardsToPlay()
//                .Select(card => Decide(game.Move(card), card, !maximize));

//            return maximize
//                ? possibleMoves.MaxElement(node => node.Points)
//                : possibleMoves.MinElement(node => node.Points);
//        }
//    }
//}