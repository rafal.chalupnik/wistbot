﻿using System.Collections.Generic;

namespace WistBot.CLI
{
    public interface ICardStack
    {
        Card Card { get; }

        void Pop();
    }

    public class CardStack : ICardStack
    {
        private readonly Stack<Card> m_cards;

        public CardStack(IEnumerable<Card> cards)
        {
            m_cards = new Stack<Card>(cards);
        }

        public Card Card => m_cards.Peek();

        public void Pop() => m_cards.Pop();
    }
}