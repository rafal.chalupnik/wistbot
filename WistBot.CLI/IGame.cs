﻿using System;
using System.Collections.Generic;
using WistBot.CLI.Constants;
using WistBot.CLI.Extensions;

namespace WistBot.CLI
{
    public interface IGame
    {
        Player CurrentPlayer { get; }

        bool HasEnded { get; }

        IPlayerState Player1 { get; }

        IPlayerState Player2 { get; }

        List<Card> GetPossibleCardsToPlay();

        void Move(Card card);
    }

    public class Game : IGame
    {
        public Player CurrentPlayer { get; internal set; }

        public Option<Card> PlayedCard { get; internal set; }

        public IPlayerState Player1 { get; }

        public IPlayerState Player2 { get; }

        public Option<Color> Triumph { get; }

        internal Game(
            Player currentPlayer, 
            Option<Card> playedCard, 
            IPlayerState player1, 
            IPlayerState player2,
            Option<Color> triumph)
        {
            CurrentPlayer = currentPlayer;
            PlayedCard = playedCard;
            Player1 = player1;
            Player2 = player2;
            Triumph = triumph;
        }

        public IPlayerState CurrentPlayerState
        {
            get
            {
                switch (CurrentPlayer)
                {
                    case Player.Player1:
                        return Player1;
                    case Player.Player2:
                        return Player2;
                    default:
                        throw new InvalidOperationException("Not defined.");
                }
            }
        }

        public bool HasEnded => GetPossibleCardsToPlay().IsEmpty();

        public List<Card> GetPossibleCardsToPlay()
            => CurrentPlayerState.GetPossibleCardsToPlay();

        public void Move(Card card)
        {
            if (!GetPossibleCardsToPlay().Contains(card))
            {
                throw new InvalidOperationException("Cannot play this card.");
            }

            CurrentPlayerState.RemoveCard(card);

            PlayedCard
                .Map(playedCard => GetWinningPlayer(playedCard, card))
                .Bind(AddPointsForPlayer);

            PlayedCard = PlayedCard
                .Map(_ => Option<Card>.None())
                .Reduce(Option<Card>.Some(card));

            CurrentPlayer = CurrentPlayer.GetOppositePlayer();
        }

        private Player GetWinningPlayer(Card card1, Card card2)
        {
            return CardWinsWithPlayedCard(card1, card2) 
                ? CurrentPlayer
                : CurrentPlayer.GetOppositePlayer();
        }

        private void AddPointsForPlayer(Player player)
        {
            switch (player)
            {
                case Player.Player1:
                    Player1.AddPoint();
                    break;
                case Player.Player2:
                    Player2.AddPoint();
                    break;
                default:
                    throw new InvalidOperationException("Invalid player");
            }
        }

        private bool CardWinsWithPlayedCard(Card card1, Card card2)
        {
            if (card1.Color == card2.Color)
            {
                return card2.Rank > card1.Rank;
            }

            return Triumph
                .Map(triumphColor => card2.Color == triumphColor)
                .Reduce(false);
        }
    }
}