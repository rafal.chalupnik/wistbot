﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WistBot.CLI
{
    public interface IPlayerCards
    {
        List<Card> GetPlayableCards(bool includeCardsOnHand);

        void RemoveCard(Card card, bool includeCardsOnHand);
    }

    public class PlayerCards : IPlayerCards
    {
        private readonly List<ICardStack> m_onTable;
        private readonly List<Card> m_onHand;

        public PlayerCards(IEnumerable<ICardStack> onTable, IEnumerable<Card> onHand)
        {
            m_onTable = onTable.ToList();
            m_onHand = onHand.ToList();
        }

        public List<Card> GetPlayableCards(bool includeCardsOnHand)
        {
            var playableCardsOnTable = m_onTable
                .Select(cardStack => cardStack.Card);

            if (includeCardsOnHand)
            {
                playableCardsOnTable = playableCardsOnTable
                    .Union(m_onHand);
            }

            return playableCardsOnTable.ToList();
        }

        public void RemoveCard(Card card, bool includeCardsOnHand)
        {
            var cardStackContainingCard = m_onTable
                .SingleOrDefault(cardStack => cardStack.Card.Equals(card));

            if (cardStackContainingCard != null)
            {
                cardStackContainingCard.Pop();
                return;
            }

            if (includeCardsOnHand)
            {
                m_onHand.Remove(card);
            }
            else
            {
                throw new ArgumentNullException("Cannot play this card.");
            }
        }
    }
}