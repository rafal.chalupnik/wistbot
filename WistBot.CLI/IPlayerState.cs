﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace WistBot.CLI
{
    public interface IPlayerState
    {
        int Points { get; }

        void AddPoint();

        List<Card> GetPossibleCardsToPlay();

        void RemoveCard(Card card);
    }

    public class PlayerState : IPlayerState
    {
        private const int c_pointsRequiredToUseCardsOnHand = 3;

        private readonly IPlayerCards m_playerCards;

        public int Points { get; private set; }

        public PlayerState(IPlayerCards cards)
        {
            m_playerCards = cards;
            Points = 0;
        }
        
        public void AddPoint()
            => Points++;

        public List<Card> GetPossibleCardsToPlay()
            => m_playerCards.GetPlayableCards(Points >= c_pointsRequiredToUseCardsOnHand);

        public void RemoveCard(Card card)
            => m_playerCards.RemoveCard(card, Points >= c_pointsRequiredToUseCardsOnHand);
    }
}