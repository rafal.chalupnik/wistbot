﻿using System;

namespace WistBot.CLI
{
    public abstract class Option<T>
    {
        public static Option<T> Some(T value)
            => new SomeImplementation<T>(value);

        public static Option<T> None()
            => new NoneImplementation<T>();

        public abstract void Bind(Action<T> action);

        public abstract Option<TNew> Map<TNew>(Func<T, TNew> mapFunc);

        public abstract T Reduce(T defaultValue);

        private class SomeImplementation<T> : Option<T>
        {
            private readonly T m_value;

            public SomeImplementation(T value)
            {
                m_value = value;
            }

            public override void Bind(Action<T> action)
                => action(m_value);

            public override Option<TNew> Map<TNew>(Func<T, TNew> mapFunc)
                => new SomeImplementation<TNew>(mapFunc(m_value));

            public override T Reduce(T defaultValue)
                => m_value;
        }

        private class NoneImplementation<T> : Option<T>
        {
            public override void Bind(Action<T> action) { }

            public override Option<TNew> Map<TNew>(Func<T, TNew> mapFunc)
                => new NoneImplementation<TNew>();

            public override T Reduce(T defaultValue)
                => defaultValue;
        }
    }
}